const express = require ('express');

const Vigenere = require('caesar-salad').Vigenere;

const app = express();

const port = 8001;

const password = 'password';

app.get('/', (req,res)=>{
    res.send('Go to another way after /decode/ or /encode/ ');
});


app.get('/encode/:text', (req,res)=>{
    let encode = Vigenere.Cipher(password).crypt(req.params.text);
    res.send(encode)
});

app.get('/decode/:text', (req,res)=>{
    let decode =Vigenere.Decipher(password).crypt(req.params.text);
    res.send(decode)
});

app.listen(port,() =>{
    console.log('We are live on http://localhost: '+ port);
});
