const express = require('express');

const app = express();

const port = 7000;

app.get('/', (req,res)=>{
    res.send('Go to another way after / ');
});

app.get('/:text', (req,res)=>{
    res.send('Echo said '+req.params.text);
});

app.listen(port,() =>{
    console.log('We are live on http://localhost: '+ port);
});

